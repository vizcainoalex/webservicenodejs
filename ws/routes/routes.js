/**
 * Created by siro on 18/07/17.
 */
var selva = require('./areasselvaticas');
var deforestacion = require('./deforestacion');
var jwt = require('jsonwebtoken');  //libreria para verificar los tokens
exports.assignRoutes = function (app) {
    app.get('/areaselvaticas', autenticacionToken,selva.getAreasSelvaticasRouter);
    app.post('/areaselvaticas', autenticacionToken,selva.addAreasSelvaticasRouter);
    app.get('/deforestacion', autenticacionToken,deforestacion.getDeforestacionRouter);
    app.post('/deforestacion', autenticacionToken,deforestacion.addDeforestacionRouter);
	app.get('/token',generarToken);
}

/*
*
*autenticacionToken: metodo para verificar si en los header esta el campo authorization
*/
function autenticacionToken(req, res, next){ 
	
	const bearerHeader = req.headers["authorization"];
	if(typeof bearerHeader !== 'undefined'){
		const bearer = bearerHeader.split(" ");
		const bearerToken = bearer[1];
		req.token = bearerToken;
		jwt.verify(bearerToken,'big_data_token',function(err,data){
			if(err){
				res.status(403).send({message: 'Token invalido'});
			}else{
				next();
			}
		});
		
	}else{
		res.status(403).send({message:'Error de token'});
	}
}

function generarToken(req, res){
const tok = {id:3};
	const token = jwt.sign({tok},'big_data_token');
	res.json({token:token})
}