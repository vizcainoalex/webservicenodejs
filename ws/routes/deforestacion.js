/**
 * Created by siro on 18/07/17.
 */

var usersDB = require('../db/desforestacion');

exports.getDeforestacionRouter = function(req, res) {
    usersDB.getDeforestacion(function(err, objecto) {
        res.send(objecto);
    });
}

exports.addDeforestacionRouter = function(req, res) {
	var country = req.body.country;
    var iso3 = req.body.iso3;
    var wdpaId = req.body.wdpaId;
	var parkName = req.body.parkName;
	var year = req.body.year;
	var outside = req.body.outside;
	var inside = req.body.inside;

    usersDB.createDeforestacion(country,iso3,wdpaId,parkName,year,outside,inside, function(err, result) {
        if(err){
			res.status(500).send({message: 'Se presento un error en la opracion'})
		}
		res.status(201).send();
    });
}