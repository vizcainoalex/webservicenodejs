/*
 * Created by davicente on 2017/10/07  
 */

var utilsDB = require('./utils');
var ObjectID = require('mongodb').ObjectID;

var db = utilsDB.getDbConnection();
var usersCollection;
db.createCollection('Deforestacion', function(err, collection) {
    if(err) {
        console.log('Error creating users collection');
    }
    usersCollection = collection;
    console.log('Collection Deforestacion created');
});

exports.createDeforestacion = function(country,iso3,wdpaId,parkName,year,outside,inside, callback) {
    usersCollection.insert({"country":country,
                            "iso3":iso3,
                            "wdpaId":wdpaId,
                            "parkName":parkName,
                            "year":year,
                            "outside":outside,
                            "inside":inside}, {w:1}, callback);
}

exports.getDeforestacion = function(callback) {
    usersCollection.find().toArray(callback);
}