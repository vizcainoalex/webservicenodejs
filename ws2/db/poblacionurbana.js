/*
 * Created by davicente on 2017/10/07  
 */

var utilsDB = require('./utils');
var ObjectID = require('mongodb').ObjectID;

var db = utilsDB.getDbConnection();
var usersCollection;
db.createCollection('PoblacionUrbana', function(err, collection) {
    if(err) {
        console.log('Error creating users collection');
    }
    usersCollection = collection;
    console.log('Collection PoblacionUrbana created');
});

exports.createPoblacionUrbana = function(country, countryCode, indicatorName, indicatorCode, a1960, a1961,a1962,a1963,a1964,a1965,a1966,
                               a1967,a1968,a1969,a1970,a1971,a1972,a1973,a1974,a1975,a1976,
                               a1977,a1978,a1979,a1980,a1981,a1982,a1983,a1984,a1985,a1986,a1987,
                               a1988,a1989,a1990,a1991,a1992,a1993,a1994,a1995,a1996,a1997,a1998,a1999,a2000, a2001,a2002,a2003,a2004,a2005,a2006,a2007,a2008,a2009,a2010,a2011,
                               a2012,a2013,a2014,a2015,a2016,a2017,callback) {
    usersCollection.insert({"country":country,
                            "countryCode":countryCode,
                            "indicatorName":indicatorName, 
                            "indicatorCode":indicatorCode, 
                            "a1960":a1960, 
                            "a1961":a1961,
                            "a1962":a1962,
                            "a1963":a1963,
                            "a1964":a1964,
                            "a1965":a1965,
                            "a1966":a1966,
                            "a1967":a1967,
                            "a1968":a1968,
                            "a1969":a1969,
                            "a1970":a1970,
                            "a1971":a1971,
                            "a1972":a1972,
                            "a1973":a1973,
                            "a1974":a1974,
                            "a1975":a1975,
                            "a1976":a1976,
                            "a1977":a1977,
                            "a1978":a1978,
                            "a1979":a1979,
                            "a1980":a1980,
                            "a1981":a1981,
                            "a1982":a1982,
                            "a1983":a1983,
                            "a1984":a1984,
                            "a1985":a1985,
                            "a1986":a1986,
                            "a1987":a1987,
                            "a1988":a1988,
                            "a1989":a1989,
                            "a1990":a1990,
                            "a1991":a1991,
                            "a1992":a1992,
                            "a1993":a1993,
                            "a1994":a1994,
                            "a1995":a1995,
                            "a1996":a1996,
                            "a1997":a1997,
                            "a1998":a1998,
                            "a1999":a1999,
                            "a2000":a2000, 
                            "a2001":a2001,
                            "a2002":a2002,
                            "a2003":a2003,
                            "a2004":a2004,
                            "a2005":a2005,
                            "a2006":a2006,
                            "a2007":a2007,
                            "a2008":a2008,
                            "a2009":a2009,
                            "a2010":a2010,
                            "a2011":a2011,
                            "a2012":a2012,
                            "a2013":a2013,
                            "a2014":a2014,
                            "a2015":a2015,
                            "a2016":a2016,
                            "a2017":a2017 }, {w:1}, callback);
}

exports.getPoblacionUrbana = function(callback) {
    usersCollection.find().toArray(callback);
}