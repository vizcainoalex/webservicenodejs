/**
 * Created by siro on 18/07/17.
 */

var usersDB = require('../db/poblacionurbana');

exports.getPoblacionUrbanaRouter = function(req, res) {
    usersDB.getPoblacionUrbana(function(err, objecto) {
        res.send(objecto);
    });
}

exports.addPoblacionUrbanaRouter = function(req, res) {
	var country = req.body.country;
    var countryCode = req.body.countryCode;
    var indicatorName = req.body.indicatorName;
	var indicatorCode = req.body.indicatorCode;
	var a1960 = req.body.a1960;
	var a1961 = req.body.a1961;
	var a1962 = req.body.a1962;
	var a1963 = req.body.a1963;
	var a1964 = req.body.a1964;
	var a1965 = req.body.a1965;
	var a1966 = req.body.a1966;
	var a1967 = req.body.a1967;
	var a1968 = req.body.a1968;
	var a1969 = req.body.a1969;
	var a1970 = req.body.a1970;
	var a1971 = req.body.a1971;
	var a1972 = req.body.a1972;
	var a1973 = req.body.a1973;
	var a1974 = req.body.a1974;
	var a1975 = req.body.a1975;
	var a1976 = req.body.a1976;
	var a1977 = req.body.a1977;
	var a1978 = req.body.a1978;
	var a1979 = req.body.a1979;
	var a1980 = req.body.a1980;
	var a1981 = req.body.a1981;
	var a1982 = req.body.a1982;
	var a1983 = req.body.a1983;
	var a1984 = req.body.a1984;
	var a1985 = req.body.a1985;
	var a1986 = req.body.a1986;
	var a1987 = req.body.a1987;
	var a1988 = req.body.a1988;
	var a1989 = req.body.a1989;
	var a1990 = req.body.a1990;
	var a1991 = req.body.a1991;
	var a1992 = req.body.a1992;
	var a1993 = req.body.a1993;
	var a1994 = req.body.a1994;
	var a1995 = req.body.a1995;
	var a1996 = req.body.a1996;
	var a1997 = req.body.a1997;
	var a1998 = req.body.a1998;
	var a1999 = req.body.a1999;
	var a2000 = req.body.a2000;
	var a2001 = req.body.a2001;
	var a2002 = req.body.a2002;
	var a2003 = req.body.a2003;
	var a2004 = req.body.a2004;
	var a2005 = req.body.a2005;
	var a2006 = req.body.a2006;
	var a2007 = req.body.a2007;
	var a2008 = req.body.a2008;
	var a2009 = req.body.a2009;
	var a2010 = req.body.a2010;
	var a2011 = req.body.a2011;
	var a2012 = req.body.a2012;
	var a2013 = req.body.a2013;
	var a2014 = req.body.a2014;
	var a2015 = req.body.a2015;
	var a2016 = req.body.a2016;
	var a2017 = req.body.a2017;

    usersDB.createPoblacionUrbana(country, countryCode, indicatorName, indicatorCode, a1960, a1961,a1962,a1963,a1964,a1965,a1966,
                               a1967,a1968,a1969,a1970,a1971,a1972,a1973,a1974,a1975,a1976,
                               a1977,a1978,a1979,a1980,a1981,a1982,a1983,a1984,a1985,a1986,a1987,
                               a1988,a1989,a1990,a1991,a1992,a1993,a1994,a1995,a1996,a1997,a1998,a1999,a2000, a2001,a2002,a2003,a2004,a2005,a2006,a2007,a2008,a2009,a2010,a2011,
                               a2012,a2013,a2014,a2015,a2016,a2017, function(err, result) {
        if(err){
			res.status(500).send({message: 'Se presento un error en la opracion'})
		}
		res.status(201).send();
    });
}


